<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<style>
body{
background: linear-gradient(white,lightblue);
text-align:center;
margin-top:200px;
}
table{
width:700px;
font-size:20px;
}
header {
	top: 0;
	background-color: #1e0033;
	color: #ffffff;
	
}

nav {
	float:left;
	padding: 15px;
}
.nav-link {
	
	color: #ffffff;
	font-size: 25px;
	margin-left: 50px;
	text-decoration: none;
	display: inline-block;
}
header {
	width: 100%;
	position: fixed;
}

</style>
</head>
<body>
<header> 
	
	<nav>
	<a class="nav-link" href="viewpesticidelist">Pesticides</a>
	 <a class="nav-link" href="scheme-list-admin">GovernmentSchemes</a>
	</nav> </header>
<h1>Pesticides List</h1>
<table align="center" border="5px">
<tr><th>Id</th><th>PesticideName</th><th>Price</th><th>Units</th><th>Update</th><th>Delete</th></tr>
<c:forEach var="b" items="${list}">
<tr>
<td>${b.id}</td>
<td>${b.name}</td>
<td>${b.price}</td>
<td>${b.units}</td>
<td><a href="editpesticide/${b.id}" class="btn btn-success">Edit</a></td>
<td><a href="deletepesticide/${b.id }" class="btn btn-danger">Delete</a></td>
</tr>
</c:forEach>
</table>
</br>
<a href="pesticidelist" class="btn btn-info">Add New Pesticide</a>
</body>
</html>