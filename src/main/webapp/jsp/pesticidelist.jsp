<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<style>
body{
	background: linear-gradient(white,lightblue);
	padding-top: 25vh;	
}

header {
	top: 0;
	background-color: #1e0033;
	color: #ffffff;
	
}
form{
	background: #fff;
}
nav {
	float:left;
	padding: 15px;
}
.nav-link {
	
	color: #ffffff;
	font-size: 25px;
	margin-left: 50px;
	text-decoration: none;
	display: inline-block;
}
header {
	width: 100%;
	position: fixed;
}
.form-container{
	border-radius: 10px;
	padding: 15px;
}

</style>
</head>
<body>
<header> 
	
	<nav>
	<a class="nav-link" href="viewpesticidelist">Pesticides</a>
	 <a class="nav-link" href="scheme-list-admin">GovernmentSchemes</a>
	</nav> </header>
	<section class="container">
<section class="row justify-content-center">
<section class="col-12 col-sm-4 col-md-6">
<form:form method="post" action="save" class="form-container" >

	<div class="form-group">
	<h4 class="text-center font-weight-bold"> Adding Pesticide </h4>
		<label>PesticideName:</label>	
			<form:input type="text" path="name" class="form-control" required="required"/><br>
		</div>
		<div class="form-group">
		<label>Price:</label>
		<form:input type="text" path="price" class="form-control" required="required"/><br>
		</div>
		<div class="form-group">
		<label>Units:</label>
		<form:input type="text" path="units" class="form-control" required="required"/><br>
		</div>
		<input type="submit" value="Save" class="btn btn-success"/>
		

</form:form>
</section>
    </section>
    </section>
</body>
</html>