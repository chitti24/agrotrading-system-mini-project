package com.agrotrading.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.agrotrading.models.Admin;
import com.agrotrading.models.Buyers;
import com.agrotrading.models.Experts;
import com.agrotrading.models.Farmers;
import com.agrotrading.models.User;
@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public boolean RegisterFarmers(Farmers farmer) {
		// TODO Auto-generated method stub
		int res=jdbcTemplate.update("insert into farmers(firstname,lastname,contactnumber,email,password,gender) values(?,?,?,?,?,?)",farmer.getFirstname(),
				farmer.getLastname(),farmer.getContactnumber(),farmer.getEmail(),
				farmer.getPassword(),farmer.getGender());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean RegisterExperts(Experts expert) {
		// TODO Auto-generated method stub
		int res=jdbcTemplate.update("insert into experts(firstname,lastname,contactnumber,email,password,gender) values(?,?,?,?,?,?)",expert.getFirstname(),
				expert.getLastname(),expert.getContactnumber(),expert.getEmail(),
				expert.getPassword(),expert.getGender());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean RegisterBuyers(Buyers buyer) {
		int res=jdbcTemplate.update("insert into buyers(firstname,lastname,contactnumber,email,password,gender) values(?,?,?,?,?,?)",buyer.getFirstname(),
				buyer.getLastname(),buyer.getContactnumber(),buyer.getEmail(),
				buyer.getPassword(),buyer.getGender());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public Farmers ValidateFarmers(String username) {
		// TODO Auto-generated method stub
		PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, username);
				
			}
		};
			return jdbcTemplate.query("select * from farmers where firstname=?", setter, new ResultSetExtractor<Farmers>() {

				@Override
				public Farmers extractData(ResultSet rs) throws SQLException, DataAccessException {
					// TODO Auto-generated method stub
					Farmers farm=null;
					if(rs.next()) {
						farm=new Farmers();
						farm.setFarmerid(rs.getInt(1));
						farm.setFirstname(rs.getString(2));
						farm.setLastname(rs.getString(3));
						farm.setContactnumber(rs.getString(4));
						farm.setEmail(rs.getString(5));
						farm.setPassword(rs.getString(6));
						farm.setGender(rs.getString(7));
						return farm;
					}
					return farm;
				}
			});
	}
	@Override
	public Experts ValidateExperts(String username) {
PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, username);
				
			}
		};
			return jdbcTemplate.query("select * from experts where firstname=?", setter, new ResultSetExtractor<Experts>() {

				@Override
				public Experts extractData(ResultSet rs) throws SQLException, DataAccessException {
					// TODO Auto-generated method stub
					Experts farm=null;
					if(rs.next()) {
						farm=new Experts();
						farm.setExpertid(rs.getInt(1));
						farm.setFirstname(rs.getString(2));
						farm.setLastname(rs.getString(3));
						farm.setContactnumber(rs.getString(4));
						farm.setEmail(rs.getString(5));
						farm.setPassword(rs.getString(6));
						farm.setGender(rs.getString(7));
						return farm;
					}
					return farm;
				}
			});
	}

	@Override
	public Buyers ValidateBuyers(String username) {
PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				// TODO Auto-generated method stub
				ps.setString(1, username);
				
			}
		};
			return jdbcTemplate.query("select * from buyers where firstname=?", setter, new ResultSetExtractor<Buyers>() {

				@Override
				public Buyers extractData(ResultSet rs) throws SQLException, DataAccessException {
					// TODO Auto-generated method stub
					Buyers farm=null;
					if(rs.next()) {
						farm=new Buyers();
						farm.setBuyerid(rs.getInt(1));
						farm.setFirstname(rs.getString(2));
						farm.setLastname(rs.getString(3));
						farm.setContactnumber(rs.getString(4));
						farm.setEmail(rs.getString(5));
						farm.setPassword(rs.getString(6));
						farm.setGender(rs.getString(7));
						return farm;
					}
					return farm;
				}
			});
	}
	
	

	
}
