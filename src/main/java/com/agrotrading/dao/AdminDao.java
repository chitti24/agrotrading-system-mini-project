package com.agrotrading.dao;

import com.agrotrading.models.Admin;

public interface AdminDao {

	public Admin findWithUsername(String username);
}
