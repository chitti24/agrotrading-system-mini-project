package com.agrotrading.dao;

import com.agrotrading.models.Buyers;
import com.agrotrading.models.Experts;
import com.agrotrading.models.Farmers;
import com.agrotrading.models.User;


public interface UserDao {

	public boolean RegisterFarmers(Farmers farmer);//for registering farmer
	public boolean RegisterExperts(Experts expert);//for registering expert
	public boolean RegisterBuyers(Buyers buyer);//for registering buyer
	
	public Farmers ValidateFarmers(String username);//for validating farmer
	public Experts ValidateExperts(String username);//for validating expert
	public Buyers ValidateBuyers(String username);//for validating buyer
	
	
	
}
