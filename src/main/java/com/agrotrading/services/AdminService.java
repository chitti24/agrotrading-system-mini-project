package com.agrotrading.services;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.agrotrading.dao.AdminDao;
import com.agrotrading.exceptions.InvalidUsernameAndPassword;
import com.agrotrading.models.Admin;

@Service
public class AdminService {

	@Autowired
	private AdminDao dao;
	
	
		
public Admin checkAdmin(String username, String password) {
		
		Admin adm=dao.findWithUsername(username);
		if(adm.getPassword().equals(password)) {
			return adm;
		}
		throw new InvalidUsernameAndPassword("Username and Password Not Matching");
}
}