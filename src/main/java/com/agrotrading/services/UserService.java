package com.agrotrading.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agrotrading.dao.UserDao;
import com.agrotrading.exceptions.InvalidUsernameAndPassword;
import com.agrotrading.models.Admin;
import com.agrotrading.models.Buyers;
import com.agrotrading.models.Experts;
import com.agrotrading.models.Farmers;
import com.agrotrading.models.User;


@Service
public class UserService {

	@Autowired
	private UserDao userdao;
	
	
	public boolean RegisterFarmer(Farmers farmer) {
		return userdao.RegisterFarmers(farmer);
	}
	
	public boolean RegisterExpert(Experts expert) {
		return userdao.RegisterExperts(expert);
	}
	
	public boolean RegisterBuyer(Buyers buyer) {
		return userdao.RegisterBuyers(buyer);
	}
	
	
public Farmers checkFarmers(String username, String password) {
	
	Farmers adm=userdao.ValidateFarmers(username);
	if(adm.getPassword().equals(password)) {
		return adm;
	}
	throw new InvalidUsernameAndPassword("Username and Password Not Matching");
}


public Experts checkExperts(String username, String password) {
	
	Experts adm=userdao.ValidateExperts(username);
	if(adm.getPassword().equals(password)) {
		return adm;
	}
	throw new InvalidUsernameAndPassword("Username and Password Not Matching");
}


public Buyers checkBuyers(String username, String password) {
	
	Buyers adm=userdao.ValidateBuyers(username);
	if(adm.getPassword().equals(password)) {
		return adm;
	}
	throw new InvalidUsernameAndPassword("Username and Password Not Matching");
}
	
}
