package com.agrotrading.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.agrotrading.dao.PesticideDao;
import com.agrotrading.models.Admin;
import com.agrotrading.models.Buyers;
import com.agrotrading.models.Experts;
import com.agrotrading.models.Farmers;
import com.agrotrading.models.Pesticides;
import com.agrotrading.models.User;
import com.agrotrading.services.AdminService;

import com.agrotrading.services.UserService;


@Controller
public class TradingController {
	@Autowired
	PesticideDao dao;
	@Autowired
	AdminService service;
	@Autowired
	UserService service1;
	
	
	@ModelAttribute("GenderList")
	public List<String> buildstate(){
		List<String> serviceMap=new ArrayList<String>();
		serviceMap.add("Select...");
		serviceMap.add("Male");
		serviceMap.add("Female");
		return serviceMap;
	}
	@ModelAttribute("Roles")
	public List<String> buildstate1(){
		List<String> serviceMap=new ArrayList<String>();
		serviceMap.add("Select...");
		serviceMap.add("Farmer");
		serviceMap.add("Wholesale Buyers");
		serviceMap.add("Experts");
		return serviceMap;
	}
	
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String FirstPage() {
		return "index";
	}
	
	@RequestMapping(value="/adminpage",method = RequestMethod.GET)
	public String showAdminForm(Model m) {
		m.addAttribute("admin",new Admin());
		return "adminpage";
	}
	 @PostMapping(value="/adminpage")
	 public String login(@RequestParam(name="username")String username, @RequestParam(name="password") String password,HttpSession
	  session,Model model) {
		 try {
			 
		 
		 Admin adm=service.checkAdmin(username, password);
		 
		 
			
			 return "adminhome";
		 }
		 
		 catch (Exception e) {
			// TODO: handle exception
		
			 model.addAttribute("mess","Entered Invalid username or password");
			 return "adminpage";
		 }
	 }
	 @RequestMapping(value="/userlogin")
		public String showForm() {
			
			return "userLogin";
		}
	
	 @RequestMapping(value="/register", method = RequestMethod.GET)
		public String addUser(Model model)
		{
			
			model.addAttribute("user",new User());
			return "UserRegister";
		}
	 @RequestMapping(value="/userRegister",method=RequestMethod.POST)
	 public String addEmployee(@Valid @ModelAttribute ("user") User user,BindingResult result,Model model) {
		 if(result.hasErrors())
			{
				System.out.println("Error");
				return "UserRegister";
			}
		 else {
			 if(user.getRole().equals("Farmer")) {
				 Farmers farm=new Farmers();
				 farm.setFirstname(user.getFirstname());
				 farm.setLastname(user.getLastname());
				 farm.setContactnumber(user.getContactnumber());
				 farm.setEmail(user.getEmail());
				 farm.setPassword(user.getPassword());
				 farm.setGender(user.getGender());
				 service1.RegisterFarmer(farm);
				 return "FarmerLogin";
				 
			 }
			 else if(user.getRole().equals("Experts")) {
				 Experts farm=new Experts();
				 farm.setFirstname(user.getFirstname());
				 farm.setLastname(user.getLastname());
				 farm.setContactnumber(user.getContactnumber());
				 farm.setEmail(user.getEmail());
				 farm.setPassword(user.getPassword());
				 farm.setGender(user.getGender());
				 service1.RegisterExpert(farm);
				 return "ExpertLogin";
				 
			 }
			 else {
				 Buyers farm=new Buyers();
				 farm.setFirstname(user.getFirstname());
				 farm.setLastname(user.getLastname());
				 farm.setContactnumber(user.getContactnumber());
				 farm.setEmail(user.getEmail());
				 farm.setPassword(user.getPassword());
				 farm.setGender(user.getGender());
				 service1.RegisterBuyer(farm);
				 return "BuyerLogin";
				 
			 }
		 }
			
		 	
			
//			return "UserLogin";
	 }
	 @PostMapping(value="/FarmerHomePage")
	 public String FarmerLogin(@RequestParam(name="username")String username, @RequestParam(name="password") String password,HttpSession
	  session,Model model) {
		 		try {
		 			
		 		
		 	 Farmers farm=service1.checkFarmers(username, password);
		 	 return "FarmerHomePage";
		 		}
			catch (Exception e) {
				// TODO: handle exception
				model.addAttribute("mess", "Username or Password is Invalid");
				return "FarmerLogin";
			}
		 }
	 
	 
	 @PostMapping(value="/BuyerHomePage")
	 public String BuyerLogin(@RequestParam(name="username")String username, @RequestParam(name="password") String password,HttpSession
	  session,Model model) {
		 		try {
		 			
		 		
		 	 Buyers farm=service1.checkBuyers(username, password);
		 	 return "BuyerHomePage";
		 		}
			catch (Exception e) {
				// TODO: handle exception
				model.addAttribute("mess", "Username or Password is Invalid");
				return "BuyerLogin";
			}
		 }
	 
	 
	 @PostMapping(value="/ExpertHomePage")
	 public String ExpertLogin(@RequestParam(name="username")String username, @RequestParam(name="password") String password,HttpSession
	  session,Model model) {
		 		try {
		 			
		 		
		 	 Experts farm=service1.checkExperts(username, password);
		 	 return "ExpertHomePage";
		 		}
			catch (Exception e) {
				// TODO: handle exception
				model.addAttribute("mess", "Username or Password is Invalid");
				return "ExpertLogin";
			}
		 }
	 
	 
	 @RequestMapping(value="/pesticidelist")
		public String showform(Model m) {
			m.addAttribute("command", new Pesticides());
			return "pesticidelist";
		}
		@RequestMapping(value="/save",method=RequestMethod.POST)
		public String save(@ModelAttribute("b")  Pesticides b) {
			
			dao.save(b);
			return "redirect:/viewpesticidelist";
		}
		@RequestMapping(value="/viewpesticidelist")
		public String viewstd(Model m) {
			List<Pesticides> list=dao.getPesticides();
			m.addAttribute("list",list);
			return "viewpesticidelist";
		}
		@RequestMapping(value="/editpesticide/{id}")
		public String edit(@PathVariable int id,Model m) {
			Pesticides b=dao.getPesticideById(id);
			m.addAttribute("command", b);
			return "editpesticidelist";
		}
		@RequestMapping(value="/editpesticide/editsave",method=RequestMethod.POST)
		public String editsave(@ModelAttribute("b") Pesticides b) {
			dao.update(b);
			return "redirect:/viewpesticidelist";
		}
		@RequestMapping(value="/deletepesticide/{id}",method=RequestMethod.GET)
		public String deletestd(@PathVariable int id) {
			dao.delete(id);
			return "redirect:/viewpesticidelist";
		}
}
