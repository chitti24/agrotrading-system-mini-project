package com.agrotrading.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.agrotrading.exceptions.InvalidUsernameAndPassword;

@ControllerAdvice
public class ExceptionHandlingController {
	
	@ExceptionHandler(InvalidUsernameAndPassword.class)
	public String HandleInvalidUsernameAndPassword(InvalidUsernameAndPassword iuap) {
	 return	iuap.getMessage();
	}

}
