package com.agrotrading.models;

public class Farmers {
	
	private int farmerid;
	private String firstname;
	private String lastname;
	private String contactnumber;
	private String email;
	private String password;
	private String gender;
	public int getFarmerid() {
		return farmerid;
	}
	public void setFarmerid(int farmerid) {
		this.farmerid = farmerid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Farmers [farmerid=" + farmerid + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", contactnumber=" + contactnumber + ", email=" + email + ", password=" + password + ", gender="
				+ gender + "]";
	}
	public Farmers(int farmerid, String firstname, String lastname, String contactnumber, String email, String password,
			String gender) {
		super();
		this.farmerid = farmerid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.contactnumber = contactnumber;
		this.email = email;
		this.password = password;
		this.gender = gender;
	}
	public Farmers() {
		super();
	}
	

}
