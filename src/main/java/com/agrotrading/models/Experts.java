package com.agrotrading.models;

public class Experts {
	
	private int expertid;
	private String firstname;
	private String lastname;
	private String contactnumber;
	private String email;
	private String password;
	private String gender;
	public int getExpertid() {
		return expertid;
	}
	public void setExpertid(int expertid) {
		this.expertid = expertid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Experts(int expertid, String firstname, String lastname, String contactnumber, String email, String password,
			String gender) {
		super();
		this.expertid = expertid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.contactnumber = contactnumber;
		this.email = email;
		this.password = password;
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Experts [expertid=" + expertid + ", firstname=" + firstname + ", lastname=" + lastname
				+ ", contactnumber=" + contactnumber + ", email=" + email + ", password=" + password + ", gender="
				+ gender + "]";
	}
	public Experts() {
		super();
	}
	

}
